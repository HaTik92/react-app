import {  Routes, Route } from 'react-router-dom';
import './App.css';
import AllCategories from './components/all_categories/allCategories';
import Cat from './components/cat/cat';
import Sidebar from './components/sidebar/sidebar';

function App() {
  return (
    <div className="App">
      <Sidebar />
      <div className="main-content">
        <Cat/>
        <Routes>
          <Route path='/all-categories' component={AllCategories} />
          <Route path='/cat' component={Cat} />
        </Routes>
      </div>
    </div>
  );
}

export default App;

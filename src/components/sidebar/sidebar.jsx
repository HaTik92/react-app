import React from "react";
import { NavLink } from "react-router-dom";
import style from "./sidebar.module.css";
const Sidebar = (props) => {
    return <>
        <div className={`sidebar ${style.sidebarContent}`}>
            <ul>
                <li><NavLink to="/">Home</NavLink></li>
                <li><NavLink to="/all-categories">All categories</NavLink></li>
                <li><NavLink to="/cat">Cat</NavLink></li>
                {/* <li><NavLink to="#">Dog</NavLink></li>
                <li><NavLink to="#">Other</NavLink></li> */}
            </ul>
        </div>
    </>
}

export default Sidebar;
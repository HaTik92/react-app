import React from "react";
import { connect } from "react-redux";
import { getImages } from "../../redux/cat-reducer";
import style from "./cat.module.css";
// import {setCats} from "../../redux/cat-reducer"

const Cat = (props) => {
    console.log(props);
    // const moreImages = (limit) => {
    //     props.getImages(limit+10,1)
    // }
    return <>
        <div className={style.catImages}>
            {props.data.images.map((el,i) => <img src={el.url} alt="" />)}
            <button 
            // onClick={moreImages(10)}
            >MORE</button>
        </div>
    </>
}
const mapStateToProps = (state) => {
    return {
        data: state.cats,
    }
}


export default connect(mapStateToProps, getImages(10,1))(Cat)

// export default Cat;
import * as axios from "axios";

const instance = axios.create({
    baseURL: 'https://api.thecatapi.com/v1/',
})

export const catsAPI = {
    getImages(limit = 10, page = 1) {
        return instance.get(`https://api.thecatapi.com/v1/images/search?limit=${limit}&page=${page}&category_ids=1`).then(response => response.data)
    },
}

import { applyMiddleware, combineReducers, createStore } from "redux";
import thunk from "redux-thunk";
import { composeWithDevTools } from "redux-devtools-extension";
import catReducer from "./cat-reducer";

let reducers = combineReducers({
    cats: catReducer,
});

const composedEnhancer = composeWithDevTools(applyMiddleware(thunk))

let store = createStore(reducers, composedEnhancer);
window.store = store
export default store;
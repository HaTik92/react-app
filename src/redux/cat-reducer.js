import { catsAPI } from "../api/api";

const SET_CATS = "SET_CATS";
const SET_MORE = "SET_MORE";

let defaultState = {
    images: [],
    limit: 10,
}
const catReducer = (state = defaultState, action) => {
    switch (action.type) {
        case SET_CATS:
            return {
                ...state,
                images: [...action.cats]
            }
        case SET_MORE:
            return {
                ...state,
                limit: action.limit
            }
        default:
            return state

    }
}

export const setCats = (cats) => ({ type: SET_CATS, cats: cats })
export const setMore = (limit) => ({ type: SET_MORE, limit })


export const getImages = (limit = 10, page=1) => {
    return (dispatch) => {
        dispatch(setMore(limit))
        catsAPI.getImages(limit, page).then(data => {
            console.log(data)
            dispatch(setCats(data))
        })
    }
}



export default catReducer;